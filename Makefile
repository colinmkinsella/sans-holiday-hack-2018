all: sansholidayhack2018writeup.pdf

sansholidayhack2018writeup.pdf: sansholidayhack2018writeup.tex
	pdflatex sansholidayhack2018writeup
	pdflatex sansholidayhack2018writeup

clean:
	rm -f *.aux
	rm -f *.bbl
	rm -f *.blg
	rm -f *.log
	rm -f *.pdf
