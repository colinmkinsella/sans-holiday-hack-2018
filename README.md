# SANS Holiday Hack 2018

[Link to Latest Paper Build](https://gitlab.com/colinmkinsella/sans-holiday-hack-2018/-/jobs/artifacts/master/download?job=build)

## Abstract
SANS has created yet another fun filled Holiday Hack. This year was modeled after a security conference called
KringleCon with a variety of speakers giving talks that helped with the individual challenges. The Holiday Hack
had a Die Hard theme, with a fitting turn of events at the end where I like to joke that Santa almost stole Christmas.
Santa claimed to have devised KringleCon and all the tests as a means to find a defender that could help him in defending
the North Pole against even the craftiest attackers, and he used his elves disguised as Toy Soldiers to help along with
Hans Gruber. SANS brought back the Cranberry Pi terminals, and this year had a means where you could submit your answers
through the web interface. The format of this paper starts with an executive summary of all the answers for the main
objectives, the narrative that evolved, a deeper dive into the objectives, followed by the write-ups for the Cranberry Pi
terminals that gave hints on how to solve the Objectives. With there being a little bit of Dev Ops this year in the
challenges, I decided to write this paper through a continuous integration process on gitlab.com where the artifact of
checking in any code is a PDF generated by this LaTeX file.